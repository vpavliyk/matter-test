**FIX:**
- Long names were incorrectly displayed in Matter apps - make names length limit by 32 characters

**NEW:**
- Make devices availability optimistic - on bridge start all restored devices are considered 'available' and are changed to 'unavailable' if they didn't "check in" via i3Net
- Update devices information on i3 state reconfiguration - if the state changes its name or entity ID, it will be updated in Matter discovery
